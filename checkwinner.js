let teams = [];

teams[0] = {
    name: 'PowerRangers',
    games: {
        firstMatch: [44, 23, 71],
        secondMatch: [85, 54, 41]
    },
    averageScores: {}
}
teams[1] = {
    name: 'FairyTails',
    games: {
        firstMatch: [65, 54, 49],
        secondMatch: [23, 34, 47]
    },
    averageScores: {}
}

//1.1
function calcAverage(scoresArr) {
    let sum = 0;
    for(let item of scoresArr) {
        sum += item;
    }
    sum /= scoresArr.length;
    return sum;
}

//1.2
for(let team of teams) {
    team.averageScores.firstMatch = calcAverage(team.games.firstMatch);
    team.averageScores.secondMatch = calcAverage(team.games.secondMatch);

    //initializing isWinner with false
    team.isWinner = false;
}

//მეორე ვარიანტი დასპრედვით

// function calcAverage(round1, round2, round3) {
//     return (round1 + round2 + round3) / 3 ;
// }

// for(let team of teams) {
//     team.averageScores.firstMatch = calcAverage(...team.games.firstMatch);
//     team.averageScores.secondMatch = calcAverage(...team.games.secondMatch);

//     //initializing isWinner with false
//     team.isWinner = false;
// }

//1.3-1.5
function checkWinner(teamOne, teamTwo) {

    teams[0].isWinner = teamOne >= teamTwo * 2;
    teams[1].isWinner = teamTwo >= teamTwo * 2;

    if (teams[0].isWinner) {
        return team[0].name;
    }
    else if(teams[1].isWinner) {
        return team[1].name;
    }
    else if(teamOne === teamTwo) {
        return `${team[0].name} and ${team[1].name}`;
    }
    else {
        return 'No one'
    }
}

let firstMatchWinner = checkWinner(teams[0].averageScores.firstMatch, teams[1].averageScores.firstMatch);
let secondMatchWinner = checkWinner(teams[0].averageScores.secondMatch, teams[1].averageScores.secondMatch);

console.log(`First Match Winner: ${firstMatchWinner}. Scores: ${teams[0].averageScores.firstMatch} vs ${teams[1].averageScores.firstMatch}`);
console.log(`Second Match Winner: ${secondMatchWinner}. Scores: ${teams[0].averageScores.secondMatch} vs ${teams[1].averageScores.secondMatch}`);



