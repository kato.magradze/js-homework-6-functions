let Ludovico = {
    bills: [22, 295, 176, 440, 37, 105, 10, 1100, 96, 52],
    tips: [],
    total: [],
    averages: {}
}

//2.1
function calcTip(bill) {
    let tip = 0;
    if((bill >= 50) && (bill <= 300)) {
        tip = (bill * 15) / 100;
    }
    else {
        tip = (bill * 20) / 100;
    }
    return tip;
}

//2.3
for(let bill of Ludovico.bills) {
    Ludovico.tips.push(calcTip(bill));
}

//2.4
for(i=0; i < Ludovico.bills.length; i++) {
    Ludovico.total.push(Ludovico.bills[i] + Ludovico.tips[i]);
}

//2.6
function calcAvg(arr) {
    let totalAvg = 0;

    for(i=0; i < arr.length; i++) {
        totalAvg += arr[i];
    }
    totalAvg /= arr.length;
    
    return totalAvg;
}

Ludovico.averages.tips = calcAvg(Ludovico.tips);
Ludovico.averages.totals = calcAvg(Ludovico.total);

console.log(Ludovico.averages.tips);
console.log(Ludovico.averages.totals);